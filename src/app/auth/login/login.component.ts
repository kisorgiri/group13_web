import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { MsgService } from './../../shared/services/msg.service';
// service is spefic task perfroming file(class)
// services must be injected

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
})

export class LoginComponent {
    user;
    submitting: boolean = false;
    rememberMe: boolean = false;
    constructor(
        public router: Router,
        public msgService: MsgService,
        public authService: AuthService
    ) {
        this.user = {
            username: '',
            password: ''
        }
    }

    askMoney() {
        return new Promise((resolve, reject) => {
            setInterval(() => {
                console.log('i am called');
                resolve('30000');
            }, 1000);
        })
    }

    watchMrRobot() {
        return Observable.create((observer) => {
            let i = 1;
            setInterval(() => {
                if (i == 8) {
                    observer.complete();
                }
                observer.next('episode ' + i);
                i++;
                // observer.complete('error')
            }, 1000)
        })
    }

    login() {
        // console.log(this.authService.upper('brodway'));
        this.authService.login(this.user)
            .subscribe(
                (data: any) => {
                    console.log('data is >>', data);
                    this.msgService.showSucess('login successfull');
                    // store data in localstorage
                    localStorage.setItem('token', data.token);
                    localStorage.setItem('user', JSON.stringify(data.user));
                    if (this.rememberMe) {
                        localStorage.setItem('remember', 'ok');
                    }
                    this.router.navigate(['/user'], {
                        queryParams: {
                            name: data.username
                        }
                    });
                },
                (err) => {
                    // if error ??? send the error data to msg service show Error method
                    this.msgService.showError(err);
                }

            )
        // 
        // 
        // this.askMoney()
        //     .then((data) => {
        //         console.log('data in promise >>', data);
        //     })
        //     .catch((err) => {
        //         console.log('error in promise >>', err);
        //     });

        // let subash = this.watchMrRobot().subscribe(
        //     (data) => {
        //         console.log('MR ROBOT for subash  >>', data);
        //     },
        //     (err) => {
        //         console.log('failure in observable >>', err);

        //     },
        //     (complete) => {
        //         console.log('complted in observable >>', complete);

        //     }
        // )

        // let milan = this.watchMrRobot().subscribe(
        //     (data) => {
        //         console.log('MR ROBOT for milan  >>', data);
        //         if (data == 'episode 3') {
        //             console.log('episode 3', data);
        //             milan.unsubscribe();
        //         }
        //     },
        //     (err) => {
        //         console.log('failure in observable >>', err);

        //     },
        //     (complete) => {
        //         console.log('complted in observable >>', complete);

        //     }
        // )

    }

    rememberMeChanged() {
        console.log('remember me clicked');
        this.rememberMe = !this.rememberMe;
    }
}

// FORM modules

// data binding
// data binding is communication between view and controller
// 3 ways of data binding
// 1 event binding  => (click,change,) any events that must call an expression and events are sorrouded by ();
// 2 property binding => [] evaluates and perfrom the property eg[disabled ]="abcd";
// 3 two way data binding  [()] ngModel keyword must be used and value must be in controller
// eg [(ngModel)]="username"
// two way databinding is synchoronization of data between view and controller 


// state of form and from -element
// valid || invalid  
// pristine ==>  the form or form element has not be interacated yet
// dirty ==> the form or form element has be interacted
// touched and untouched  || touchede focus out || foucesd

//revise of sub module
// root module >> which should not contain any other component other than root component
// main routing file >>> this file is used for sub routing(lazy loading)
// every sub module must be imported in imports of main moudle
// 
// sub module can be used as in independant module so complete work within a module must be done for submodule
// thirdparty module used in main module to show toast message

