import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable() // decorator to define class of service
export class AuthService {
    url: string;
    constructor(
        public http: HttpClient
    ) {
        this.url = environment.Base_URL + 'auth/';
    }
    upper(str: string) {
        return str.toUpperCase();
    }

    login(data: any) {
        // return Observable.create((observer) => {
        //     this.http.post(this.url + 'login', data, {
        //         headers: new HttpHeaders({
        //             'Content-Type': 'applicaiton/json'
        //         })
        //     })
        //         .subscribe(
        //             (data) => {
        //                 observer.next(data);
        //             },
        //             (error) => {
        //                 observer.error(error);
        //             }
        //         )
        // })

        return this.http.post(this.url + 'login', data, this.getOptions())



    }

    register(data: any) {
        return this.http.post(this.url + 'register', data, this.getOptions())
    }

    getOptions() {
        let headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        }
        return headers;
    }

    forgotPassword(email: string) {
        // call with email in API
        return this.http.post(this.url + 'forgot-password', { email: email }, this.getOptions());
    }
    resetPassword(data: any) {
        // call with reset API
        return this.http.post(this.url + 'reset-password/'+data.username, data, this.getOptions());

    }
    // service is a file where we use repeatedely used speic task perfroming logic
    // service in angular is used to call Backend API
}