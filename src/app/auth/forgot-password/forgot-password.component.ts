import { Component, OnInit } from '@angular/core';
import { MsgService } from 'src/app/shared/services/msg.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  submitting: boolean = false;
  email: string; // 
  constructor(public msgService: MsgService,
    public authService: AuthService,
    public router: Router) { }

  ngOnInit() {
  }

  submit() {
    this.submitting = true;
    this.authService.forgotPassword(this.email)
      .subscribe(data => {
        this.msgService.showInfo('Password reset link sent to your email please check your inbox');
        this.router.navigate(['/auth/login']);
      }, error => {
        this.submitting = false;
        this.msgService.showError(error);
      })
  }

}
