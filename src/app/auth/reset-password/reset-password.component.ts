import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { MsgService } from 'src/app/shared/services/msg.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  data;
  constructor(
    public router: Router,
    public activeRouter: ActivatedRoute,
    public authService: AuthService,
    public msgService: MsgService
  ) {
    this.data = {
      password: '',
      confirmPassword: '',
      username: this.activeRouter.snapshot.params['token']
    }
  }

  ngOnInit() {
  }

  submit() {
    this.authService.resetPassword(this.data)
      .subscribe(data => {
        this.msgService.showInfo('password reset successfull please login');
        this.router.navigate(['/auth/login']);
      },
        error => {
          this.msgService.showError(error);
        })
  }

}
