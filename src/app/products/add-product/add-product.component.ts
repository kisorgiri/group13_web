import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MsgService } from 'src/app/shared/services/msg.service';
import { ProductService } from '../services/product.service';
import { Product } from '../models/product.model';
import { UplaodService } from 'src/app/shared/services/upload.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  submitting: boolean = false;
  product;
  filesToUpload = [];
  url;
  constructor(
    public router: Router,
    public msgService: MsgService,
    public productService: ProductService,
    public uplaodService: UplaodService,
  ) {
    this.product = new Product({});
    this.url = environment.Base_URL + 'product'
  }

  ngOnInit() {
  }

  submit() {
    this.submitting = true;
    this.uplaodService.upload(this.product, this.filesToUpload, 'POST', this.url)
      .subscribe(
        data => {
          this.msgService.showSucess('Product added successfully');
          this.router.navigate(['/product/list']);
        },
        error => {
          this.submitting = false;
          this.msgService.showError(error);
        }
      )
  }

  fileChanged(ev) {
    console.log('ev', ev);
    this.filesToUpload = ev.target.files;
  }

}
