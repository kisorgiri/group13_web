export class Product {
    name: string;
    brand: string;
    category: string;
    price: number;
    color: string;
    body: string;
    quantity: string;
    warrentyStatus: boolean;
    warrentyPeriod: string;
    size: string;
    description: string;
    tags: string;
    manuDate: string;
    expiryDate: string;
    _id: string;
    minPrice: number;
    maxPrice: number;

    constructor(details: any) {

        this.name = details.name || '';
        this.brand = details.brand || '';
        this.category = details.category || '';
        this.price = details.price || '';
        this.color = details.color || '';
        this.body = details.body || '';
        this.quantity = details.quantity || '';
        this.warrentyPeriod = details.warrentyPeriod || '';
        this.warrentyStatus = details.warrentyStatus || '';
        this.size = details.size || '';
        this.description = details.description || '';
        this.tags = details.tags || '';
        this.manuDate = details.manuDate || '';
        this.expiryDate = details.expiryDate || '';
        this._id = details._id || '';
        this.minPrice = details.minPrice || '';
        this.maxPrice = details.maxPrice || '';
    }
}