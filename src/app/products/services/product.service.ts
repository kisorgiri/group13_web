import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product.model';
import { Observable } from 'rxjs';


@Injectable()
export class ProductService {
    url: string;
    constructor(public http: HttpClient) {
        this.url = environment.Base_URL + 'product';

    }

    add(data: Product) {
        return this.http.post(this.url, data, this.getOptions());
    }
    get() {
        return this.http.get(this.url, this.getOptions());

    }
    getById(id: string) {
        // return this.http.get(this.url + '/' + id, this.getOptions());
        return this.http.get(`${this.url}/${id}`, this.getOptions());
    }
    update(id: string, data: Product) {
        return this.http.put(`${this.url}/${id}`, data, this.getOptions());

    }

    remove(id) {
        return this.http.delete(`${this.url}/${id}`, this.getOptions());

    }
    search(condition: Product) {
        return this.http.post(`${this.url}/search`, condition, this.getOptions());

    }

    upload(data: Product, files: Array<any>, method) {
        console.log('data ins ervice ,', data);
        console.log('files ins ervice ,', files);
        return Observable.create((observer) => {
            const xhr = new XMLHttpRequest();
            const formData = new FormData();

            if (files.length) {
                formData.append('img', files[0], files[0].name);
            }
            for (let key in data) {
                formData.append(key, data[key]);
            }

            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        observer.next(xhr.response);
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            let url = `${this.url}?token=${localStorage.getItem('token')}`
            if (method == 'PUT') {
                url = `${this.url}/${data._id}?token=${localStorage.getItem('token')}`
            }
            xhr.open(method, url, true);
            xhr.send(formData);
        })

    }


    getOptions() {
        let headers = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': localStorage.getItem('token')
            })
        }
        return headers;
    }
}