import { Component, OnInit } from '@angular/core';
import { ProductService } from '../services/product.service';
import { MsgService } from 'src/app/shared/services/msg.service';
import { Router, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  productId;
  product;
  loading: boolean = false;
  submitting: boolean = false;
  imgUrl: string;
  uploadArray = [];
  constructor(
    public productService: ProductService,
    public msgService: MsgService,
    public router: Router,
    public activeRoute: ActivatedRoute
  ) {
    this.imgUrl = environment.Img_URL;
  }

  ngOnInit() {
    this.loading = true;
    this.productId = this.activeRoute.snapshot.params['id'];
    this.productService.getById(this.productId)
      .subscribe(
        (data: any) => {
          this.loading = false;
          console.log('data is here >>>', data);
          this.product = data;
          this.product.tags = data.tags.join(',');
        },
        error => {
          this.loading = false;
          this.msgService.showError(error);
        }
      )
  }

  submit() {
    this.submitting = true;
    this.productService.upload(this.product, this.uploadArray, 'PUT')
      .subscribe(data => {
        this.msgService.showSucess('Product updated successfully');
        this.router.navigate(['/product/list']);
      },
        error => {
          this.submitting = false;
          this.msgService.showError(error);
        })
  }

  fileChanged(ev) {
    this.uploadArray = ev.target.files;
  }

}
