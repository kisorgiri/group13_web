import { Component, OnInit, Input } from '@angular/core';
import { MsgService } from 'src/app/shared/services/msg.service';
import { ProductService } from '../services/product.service';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.css']
})
export class ListProductComponent implements OnInit {
  products;
  loading: boolean = false;
  imgUrl: string;
  @Input() data;
  constructor(
    public msgService: MsgService,
    public productService: ProductService,
    public router: Router
  ) {
    this.imgUrl = environment.Img_URL;
  }

  ngOnInit() {
    console.log('data is >>', this.data);
    if (!this.data) {
      this.loading = true;
      this.productService.get()
        .subscribe(
          data => {
            this.loading = false;
            console.log('all data of products >>', data);
            this.products = data;
          },
          error => {
            this.loading = false;
            this.msgService.showError(error);
          }
        )
    } else {
      this.products = this.data;
    }

  }

  removeProduct(id, i) {
    let removeConfirm = confirm("Are you sure to delete?");
    if (removeConfirm) {
      console.log('remove from database');
      this.productService.remove(id)
        .subscribe(
          (data) => {
            this.msgService.showSucess('Product Deleted');
            this.products.splice(i, 1);
          },
          (error) => {
            this.msgService.showError(error);
          })
    }
  }

}
