import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product.model';
import { MsgService } from 'src/app/shared/services/msg.service';
import { ProductService } from '../services/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-product',
  templateUrl: './search-product.component.html',
  styleUrls: ['./search-product.component.css']
})
export class SearchProductComponent implements OnInit {
  submitting: boolean = false;
  product;
  categories = [];
  showName: boolean = false;
  allProducts = [];
  names = [];
  multipleDateRange: boolean = false;
  result: boolean = false;
  results = [];
  constructor(public msgService: MsgService,
    public productService: ProductService,
    public router: Router
  ) {
    this.product = new Product({});
  }

  ngOnInit() {
    this.productService.get()
      .subscribe((data: any) => {
        this.allProducts = data;
        data.forEach((item, i) => {
          if (this.categories.indexOf(item.category) == -1) {
            this.categories.push(item.category);
          }
        });
      },
        error => {
          this.msgService.showError(error);
        })
  }

  search() {
    console.log('this.product to date ', this.product.toDate);
    if (!this.product.toDate) {
      this.product.toDate = this.product.fromDate;
    }
    this.submitting = true;
    this.productService.search(this.product)
      .subscribe((data: any) => {
        this.submitting = false;
        if (data.length) {
          this.result = true;
          this.results = data;
          console.log('search data here >>', data)
        } else {
          this.msgService.showInfo('No any product matched your search query')
        }
      }, error => {
        this.submitting = false;
        this.msgService.showError(error);
      })
  }

  categoryChanged(val) {
    console.log('val is >>', val);
    console.log('binding value is >>', this.product.category);
    this.names = this.allProducts.filter((item, i) => {
      if (item.category == val) {
        return true;
      }
    });
    this.showName = true;
  }
  changeMultipleDateRange() {

    this.multipleDateRange = !this.multipleDateRange;
    if (!this.multipleDateRange) {
      this.product.toDate = null;
    }
  }

}
