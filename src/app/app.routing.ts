import { NgModule } from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './shared/page-not-found/page-not-found.component';

// routing configuration
const appRoute: Routes = [
    {
        path: '',
        redirectTo: '/auth/login',
        pathMatch: 'full'
    },
    {
        path: 'auth',
        loadChildren: './auth/auth.module#AuthModule'
    },
    {
        path: 'user',
        loadChildren: './users/users.module#UsersModule'
    },
    {
        path: 'product',
        loadChildren: './products/products.module#ProductsModule'
    },
    {
        path: '**',
        component: PageNotFoundComponent
    }
]
@NgModule({
    imports: [RouterModule.forRoot(appRoute)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}

// summarize
// routing
// routermodule ==> must be injected inside imports of module
// configuration block using routes array type basic property path , component
// routermodule.forRoot(configuration  block)

// router-outlet directive is used as an placeholder to load components according to path
// navigation ===> template routerLink directive routerLink='/path_registerd_in_config_block'
//          ===> controller Router must be injected in constructor to add in class(this) and the use .navigate method eg this.router.navigate(['/path'])

// RouterModule
// Routes
// router-outlet
// routerLink
// router
// activatedRoute ==> it is used to take value from url either / or ?
// for / activaRoute.snapshot.params['id,token']
// queryParams. activeRoute.queryParams.subscribe((data)=>{
//     data is object of query key value pairs
// })