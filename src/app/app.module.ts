import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { SharedModule } from './shared/shared.module';
import { ToastrModule } from 'ngx-toastr';
import { ProductsModule } from './products/products.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    AuthModule,
    UsersModule,
    SharedModule,
    ProductsModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent] // root component(root component is required to start angular application)
})
export class AppModule { }


// glossary
// module
// component
// service
// pipe
// directive
// above mentioned are class in angular

// decorator
//selector
// meta data 

// decorator ===> decorator is function that define class using meta data
// every function that is called with @ prefix is decorator

// meta data is collection of properties and values that define class
// meta data are differenct accordingly with decorator

// @ngModule decorator
//  the properties of meta data of ngModule decorator

// declarations ==> it holds an array where we place all the components , pipes,directives
//  angular js aafai ma diff diff module bata baneko cha. 
// to start angular application we need at least one module
// one module module is root module for angular application
// imports ===> imports sectinos will hold all the modules
  // modules can be 
    // inbult module eg formsMoudle,rotuerModule,browserModule
    // third party module , module form npmjs
    // own module other then root module
  //  as at least one module is required by angular appplication to start we need at least one componet to get started
  // root component is necessary to get started with angular application
  // bootstrap ===> this sections will take the root component (it will specifiy the root component)
  // only root moudle will have bootstrap properites

  // providers ? provider will hold services
  // exports , entryComponents


  // selector 
  /// selector is a properties used by @component decorator