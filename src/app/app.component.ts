import { Component } from '@angular/core';
// import syntax is es6 way of calling another file (require in es5)
// import { name, address, test} from 'path same as of require' ;//welcome();
// import * as test from 'localtion sdlfjdlf';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // controller
  title = 'group13-web';
  loggedIn: boolean = false;
  constructor() {
    if (localStorage.getItem('remember') && localStorage.getItem('token')) {
      console.log('navigate to home || dahsboard');
    }
    if (localStorage.getItem('token')) {
      this.loggedIn = true;
    }
  }

  isLoggedIn() {
    if (localStorage.getItem('token')) {
      return true;
    }
    else {
      return false;
    }
  }
}

// meta data of component
// selector is part of component decorator which will carry entire component
// eg app-root app-root will carry entire app component with meta defined on it
// slectors are used as an html element,
// 
