import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';

@Injectable()
export class UplaodService {


    upload(data: any, files: Array<any>, method, URL) {
        console.log('data ins ervice ,', data);
        console.log('files ins ervice ,', files);
        return Observable.create((observer) => {
            const xhr = new XMLHttpRequest();
            const formData = new FormData();

            if (files.length) {
                formData.append('img', files[0], files[0].name);
            }
            for (let key in data) {
                formData.append(key, data[key]);
            }

            xhr.onreadystatechange = () => {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        observer.next(xhr.response);
                    } else {
                        observer.error(xhr.response);
                    }
                }
            }
            let url = `${URL}?token=${localStorage.getItem('token')}`
            if (method == 'PUT') {
                url = `${URL}/${data._id}?token=${localStorage.getItem('token')}`
            }
            xhr.open(method, url, true);
            xhr.send(formData);
        })

    }
}