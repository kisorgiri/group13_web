import { Injectable } from "@angular/core";
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class MsgService {
    constructor(public toastr: ToastrService) {

    }
    showSucess(msg: string) {
        this.toastr.success(msg);
    }

    showInfo(msg: string) {
        this.toastr.info(msg);
    }

    showWarnings(msg: string) {
        this.toastr.warning(msg);
    }

    showError(err: any) {
        debugger;// debugger will pause the applicationa and we can see the data and control
        // 
        // use this block as error handler
        // pass every possible error from you application here
        // parse the error data and show appropriate error message

        if (err.error) {
            this.error(err.error.message);
        }
        

    }
    private error(errMsg: string) {
        this.toastr.error(errMsg);
    }


}