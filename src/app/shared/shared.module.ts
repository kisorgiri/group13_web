import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MsgService } from './services/msg.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { LoaderComponent } from './loader/loader.component';
import { UplaodService } from './services/upload.service';

@NgModule({
  declarations: [
    PageNotFoundComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    LoaderComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  providers: [MsgService, UplaodService], // service has global scope
  // rest have local scope
  exports: [HeaderComponent, FooterComponent, SidebarComponent, LoaderComponent]
})
export class SharedModule { }
