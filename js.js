
// object short hand properties
function welcome() {
    var name = 'ram';
    var address = 'bkt'
    var student = {
        name,
        address,
        test: 'test',
        abcd: 'abcd'
    }
    return student;

}

var { name, address, test } = require() ;//welcome();
console.log('result >>', name);
console.log('result >>', address);
console.log('result >>', test);